@ECHO OFF
title TypeBeat Machines - Preview Generator
color 3D

:Start
ECHO '
ECHO '
ECHO Name of input .wav (MUST BE A .WAV, but don't include .wav in the name)
set /p audioname=
ECHO '
ECHO You set the Audio Input too %audioname%.wav
ECHO '
ECHO What number of seconds into the track do you want to start the preview?
ECHO (This needs to be at least 45 seconds less than the length of the song)
set /p audiolength=
ECHO '
ECHO '
ECHO Are you ready to create the preview for tracks for %audioIn2%?
ECHO '
ECHO 1. Yes (any key)
ECHO 2. No - Return to menu
set /p input=
if %input% == 2 goto start

set /a audiolengthstop=%audiolength%+45

md %audioname%-Package

FFMPEG -i inputfiles/%audioname%.wav -acodec libmp3lame -b:a 320k %audioname%-Package/%audioname%-320k.mp3
FFMPEG -i inputfiles/%audioname%.wav -acodec libmp3lame -b:a 128k %audioname%-Package/%audioname%-128k.mp3
FFMPEG -i inputfiles/%audioname%.wav -ss %audiolength% -to %audiolengthstop% inputfiles/%audioname%short.wav
FFMPEG -i inputfiles/%audioname%short.wav -i inputfiles/audiotag45sec.wav -filter_complex amerge=inputs=2 -ac 2 -b:a 128k %audioname%-Package/%audioname%Preview.mp3
move inputfiles\%audioname%.wav %audioname%-Package\
cd inputfiles
del %audioname%short.wav
cd..

goto Success


:Success
ECHO '
ECHO '
ECHO You Successfully Created the Preview Files!!!
PAUSE
goto Exit


:Exit
exit


