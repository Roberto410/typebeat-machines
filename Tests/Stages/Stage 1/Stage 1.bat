@ECHO OFF
title Stage 1
:menu
ECHO '
ECHO '
ECHO Get Ready to CONVERT!!!
ECHO '
ECHO 1. Run Test 2
ECHO 2. Set Audio In Out File
ECHO 3. Audio In Out File Names
ECHO 4. Convert WAV to RAW pcm_s16be
ECHO 9. Exit
set /p input=
if %input% == 1 goto Test2
if %input% == 2 goto SetAudioInOut1
if %input% == 3 goto Audio1InOutNames
if %input% == 4 goto convWavToRaw
if %input% == 9 goto Exit
goto menu

:convWavToRaw
ECHO '
ECHO '
ECHO Name your input .wav (MUST BE A .WAV, but don't include .wav in the name)
set /p audioIn2=
ECHO '
ECHO You set the Audio Input too %audioIn2%.wav
ECHO '
ECHO Name your output (no file extention)
set /p audioOut2=
ECHO '
ECHO You set the Audio Input too %audioOut2%.raw
pause
ECHO '
ECHO Are you ready to convert %audioIn2%.wav into %audioOut2%.raw?
ECHO '
ECHO 1. Yes (any key)
ECHO 2. No - Return to menu
set /p input=
if %input% == 2 goto menu
ffmpeg -i %audioIn2%.wav -f s16be -acodec pcm_s16be %audioOut2%.raw
goto convSuccess

:ConvSuccess
ECHO '
ECHO '
ECHO You Successfully Converted!!!
PAUSE
goto menu


:SetAudioInOut1
ECHO '
ECHO '
ECHO Name your input file
set /p audioIn1=
ECHO '
ECHO You set the Audio Input too %audioIn1%
ECHO '
ECHO Set the Output File
set /p audioOut1=
ECHO '
ECHO You set the Audio Input too %audioOut1%
pause
goto menu

:Audio1InOutNames
ECHO '
ECHO The Audio file 1nput 1 is set too %audioIn1%
ECHO The Audio file output 1 is set too %audioOut1%
ECHO '
ECHO 1. Change
ECHO 2. Back to menu
set /p input=
if %input% == 1 goto SetAudioInOut1
goto menu


:Test2
ffmpeg -i i1.wav -f s16be -acodec pcm_s16be i1.raw
ffmpeg -i i2.wav -f s16be -acodec pcm_s16be i2.raw
ffmpeg -i i3.wav -f s16be -acodec pcm_s16be i3.raw
ffmpeg -i i4.wav -f s16be -acodec pcm_s16be i4.raw
ffmpeg -i i5.wav -f s16be -acodec pcm_s16be i5.raw
copy /b i1.raw+i2.raw+i3.raw+i4.raw+i5.raw i6.raw
ffmpeg -f s16be -ar 44.1k -ac 2 -i i6.raw i6.wav
goto outputdisplayTest2

:outputdisplayTest2
ECHO '
ECHO '
ffmpeg -i i6.wav
ECHO '
ECHO '
ECHO You Successfully Converted!!!
PAUSE
goto menu

:exit
exit