@ECHO OFF
title TypeBeat Machines Test 5
color 5A
:menu
Pause
ffmpeg -i input/shakes.wav -filter:a "volume=0" input/shakesquiet.wav
ffmpeg -stream_loop 1 -i input/shakesquiet.wav input/loops/shakesquietloop.wav
ffmpeg -stream_loop 13 -i input/shakes.wav input/loops/shakesloop.wav
ffmpeg -i input/loops/shakesloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/shakes/2shakesloop.raw
ffmpeg -i input/loops/shakesquietloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/shakes/1shakesquietloop.raw
type "input\loops\loopraws\shakes\*.raw" > input/raws/shakes/shakesstemraw.raw
ffmpeg -f s16be -ar 44.1k -ac 2 -i input/raws/shakes/shakesstemraw.raw input/stems/shakesstemnolp.wav
ffmpeg -i input/stems/shakesstemnolp.wav -af "highpass=f=200" input/stems/shakesstem.wav


ffmpeg -i input/drums.wav -filter:a "volume=0" input/drumsquiet.wav
ffmpeg -stream_loop 3 -i input/drumsquiet.wav input/loops/drumsquietloop.wav
ffmpeg -stream_loop 7 -i input/drums.wav input/loops/drumsloop.wav
ffmpeg -i input/loops/drumsloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/drums/2drumsloop.raw
ffmpeg -i input/loops/drumsquietloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/drums/1drumquietloop.raw
ffmpeg -i input/loops/drumsquietloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/drums/3drumquietloop.raw
type "input\loops\loopraws\drums\*.raw" > input/raws/drums/drumsstemraw.raw
ffmpeg -f s16be -ar 44.1k -ac 2 -i input/raws/drums/drumsstemraw.raw input/stems/drumsstem.wav


ffmpeg -i input/bass.wav -filter:a "volume=0" input/loops/bassquietloop.wav
ffmpeg -stream_loop 1 -i input/bass.wav input/loops/bassloop.wav
ffmpeg -i input/loops/bassloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/bass/2bassloop.raw
ffmpeg -i input/loops/bassquietloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/bass/1bassquietloop.raw
ffmpeg -i input/loops/bassquietloop.wav -f s16le -acodec pcm_s16be input/loops/loopraws/bass/3bassquietloop.raw
type "input\loops\loopraws\bass\*.raw" > input/raws/bass/bassstemraw.raw
ffmpeg -f s16be -ar 44.1k -ac 2 -i input/raws/bass/bassstemraw.raw input/stems/bassstem.wav


ffmpeg -stream_loop 3 -i input/main.wav input/stems/mainstemnohp.wav
ffmpeg -i input/stems/mainstemnohp.wav -af "highpass=f=120" input/stems/mainstem.wav


ffmpeg -i input/stems/mainstem.wav -i input/stems/shakesstem.wav -i input/stems/drumsstem.wav -i input/stems/bassstem.wav -filter_complex amerge=inputs=4 -ac 2 output.wav

pause