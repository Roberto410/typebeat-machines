@ECHO OFF
title TypeBeat Machines Alpha 0.1
color 5A
:menu
ECHO '
ECHO '
ECHO Get Ready to CONVERT!!!
ECHO '
ECHO 1. Get Sample Info
ECHO 2. Convert WAV to RAW pcm_s16be
ECHO 3. Convert RAW to WAV s16be
ECHO 4. Loop Concat Samples (e.g. i1, i2, i3 naming)
ECHO 5. 
ECHO 9. Exit
set /p input=
if %input% == 1 goto GetSampleInfo
if %input% == 2 goto ConvWavToRaw
if %input% == 3 goto ConvRawToWav
if %input% == 4 goto LoopConcatFull
if %input% == 9 goto Exit
goto menu

:ConvWavToRaw
ECHO '
ECHO '
ECHO Name your input .wav (MUST BE A .WAV, but don't include .wav in the name)
set /p audioIn2=
ECHO '
ECHO You set the Audio Input too %audioIn2%.wav
ECHO '
ECHO Name your output (no file extention)
set /p audioOut2=
ECHO '
ECHO You set the Audio Output too %audioOut2%.raw
pause
ECHO '
ECHO Are you ready to convert %audioIn2%.wav into %audioOut2%.raw?
ECHO '
ECHO 1. Yes (any key)
ECHO 2. No - Return to menu
set /p input=
if %input% == 2 goto menu
ffmpeg -i %audioIn2%.wav -f s16be -acodec pcm_s16be %audioOut2%.raw
goto convSuccess

:ConvRawToWav
ECHO '
ECHO '
ECHO Name your input .raw (MUST BE A .raw, but don't include .raw in the name)
set /p audioIn2=
ECHO '
ECHO You set the Audio Input too %audioIn2%.raw
ECHO '
ECHO Name your output (no file extention)
set /p audioOut2=
ECHO '
ECHO You set the Audio Output too %audioOut2%.wav
ECHO '
ECHO set the sample rate (e.g. 44.1k)
set /p sampleRate=
ECHO '
ECHO You set the Sample Rate too %sampleRate%
pause
ECHO '
ECHO Are you ready to convert %audioIn2%.raw into %audioOut2%.Wav? at a sample rate of %sampleRate%?
ECHO '
ECHO 1. Yes (any key)
ECHO 2. No - Return to menu
set /p input=
if %input% == 2 goto menu
ffmpeg -f s16be -ar %sampleRate% -ac 2 -i %audioIn2%.raw %audioOut2%.wav
goto convSuccess

:LoopConcatFull
ECHO '
ECHO Set the beggining characters of your input files (e.g. samp3 the [samp] part)
set /p begChar=
ECHO '
ECHO You Set the beggining characters of your input files too %begChar%1.wav
ECHO '
ECHO Name your output (no file extention)
set /p audioOut2=
ECHO '
ECHO You set the Audio Output too %audioOut2%.Wav
ECHO '
ECHO set the sample rate (e.g. 44.1k)
set /p sampleRate=
ECHO '
ECHO You set the Sample Rate too %sampleRate%
ECHO '
ECHO How many samples to loop through?
set /p sampNum=
ECHO '
set sampNum = %sampNum%
ECHO You set the number of samples to loop through too %sampNum%
ECHO '
ECHO Are you ready to convert %begChar%.wav into %audioOut2%.Wav?
ECHO '
pause
FOR /L %%i IN (1,1,%sampNum%) DO (
  ECHO %%i
  ffmpeg -i %begChar%%%i.wav -f s16be -acodec pcm_s16be %begChar%%%i.raw
)
type *.raw > 1%audioOut2%.raw
ffmpeg -f s16be -ar %sampleRate% -ac 2 -i 1%audioOut2%.raw %audioOut2%.wav
goto ConvSuccess

:ConvSuccess
ECHO '
ECHO '
ECHO You Successfully Converted!!!
PAUSE
goto menu

:GetSampleInfo
ECHO '
ECHO '
ECHO Name your input sample (Make sure you include file type e.g. file.wav)
set /p audioIn1=
FFMPEG -i %audioIn1%
ECHO '
PAUSE
goto menu


:exit
exit